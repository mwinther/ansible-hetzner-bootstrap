service {
  name = "upstream"
  id = "upstream-1"
  port = 4000

  connect {
    sidecar_service {}
  }

  check {
    id       = "upstream-check"
    http     = "http://localhost:4000/health"
    method   = "GET"
    interval = "30s"
    timeout  = "1s"
  }
}
