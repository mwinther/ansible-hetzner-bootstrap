job "fakedash" {
  datacenters = ["dc1"]

  group "upstream" {
    network {
      mode = "bridge"
      port "api" { to = 5000 }
    }

    service {
      name = "upstream-api"
      port = "api"

      check {
        type = "http"
        port = "api"
        path = "health"
        interval = "15s"
        timeout = "1s"
      }

      connect {
        sidecar_service {
          proxy {
            local_service_port = 5000
          }
        }
      }
    }

    task "api" {
      driver = "docker"

      config {
        image = "10.0.0.2:5000/nicholasjackson/fake-service:v0.25.1"
      }

      env {
        NAME = "Backend"
        LISTEN_ADDR = "0.0.0.0:5000"
        MESSAGE = "${NOMAD_ALLOC_ID}"
      }
    }
  }

  group "frontend" {
    network {
      mode = "bridge"

      port "web" {
        to     = 9090
      }
    }

    service {
      name = "frontend-web"
      port = "web"

      check {
        type = "http"
        port = "web"
        path = "health"
        interval = "15s"
        timeout = "1s"
      }

      connect {
        sidecar_service {
          proxy {
            upstreams {
              destination_name = "upstream-api"
              local_bind_port = 8080
            }
          }
        }
      }
    }

    task "web" {
      driver = "docker"

      config {
        image = "10.0.0.2:5000/nicholasjackson/fake-service:v0.25.1"
      }

      env {
        UPSTREAM_URIS = "http://${NOMAD_UPSTREAM_ADDR_upstream_api}"
        NAME = "Frontend"
        MESSAGE = "${NOMAD_ALLOC_ID}"
      }
    }
  }
}
