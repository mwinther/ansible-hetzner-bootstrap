service {
  name = "frontend"
  port = 7070

  connect {
    sidecar_service {
      proxy {
        upstreams = [
          {
            destination_name = "upstream"
            local_bind_port  = 4000
          }
        ]
      }
    }
  }

  check {
    id       = "frontend-check"
    http     = "http://localhost:7070/health"
    method   = "GET"
    interval = "30s"
    timeout  = "1s"
  }
}
