---
- name: Setup nomad server
  hosts: nomad_server

  vars:
    config_dir: "{{ nomad_config_dir |d('/etc/nomad.d') }}"
    env_file: "{{ nomad_env_file |d('nomad.env') }}"
    vault_addr: "{{ vault_uri |d('http://127.0.0.1:8200') }}"
    anonymous_policy: "{{ nomad_anonymous_policy |d(false) }}"
    acl_enabled: "{{ nomad_acl_enabled |d(false) }}"
    server_policy_in_consul: "{{ nomad_server_policy_in_consul }}"
    client_policy_in_consul: "{{ nomad_client_policy_in_consul }}"
    nomad_cluster_firewall_ports: "{{ cluster_firewall_ports }}"
    configure_firewalls: "{{ internal_configure_firewalls |d(false) }}"
    consul_management_token: >-
      "{{ _initial_management_token.data.data.key }}"

  tasks:
    - name: Install nomad package
      package:
        name: nomad
        state: present

    - name: Import nomad variables
      include_vars: nomad_server_vars.yaml

    - name: Debug output consul token
      copy:
        dest: /tmp/consul_mgmt_token
        content: "{{ consul_management_token[1:-1] }}"

    # FIXME: Handle policy already existing (500)
    - name: Create nomad server policy in consul
      block:
        - name: Put server policy in consul API
          uri:
            url: https://127.0.0.1:8501/v1/acl/policy
            method: PUT
            headers:
              Authorization: "Bearer {{ consul_management_token[1:-1] }}"
            body_format: json
            body:
              Name: "{{ nomad_server_policy_name }}"
              Description: "Nomad server policy"
              Rules: "{{ nomad_server_policy_rules | to_json }}"
            return_content: true
          register: server_policy_results

        - name: Create nomad server token in consul
          uri:
            url: https://127.0.0.1:8501/v1/acl/token
            method: PUT
            headers:
              Authorization: "Bearer {{ consul_management_token[1:-1] }}"
            body_format: json
            body:
              Description: "Nomad server token"
              Policies:
                - Name: "{{ nomad_server_policy_name }}"
            return_content: true
          register: server_token_results

        - name: Write nomad server token to Vault
          uri:
            url: "{{ vault_addr }}/v1/bootstrapper_kv/data/nomad/server"
            method: POST
            headers:
              Authorization: "Bearer {{ bootstrapper_token }}"
            body_format: json
            body:
              data:
                token: "{{ server_token_results.json.SecretID }}"

        - name: Debug output
          copy:
            content: "{{ server_token_results }}"
            dest: /tmp/server_token_results

        - name: Put client policy in consul API
          uri:
            url: https://127.0.0.1:8501/v1/acl/policy
            method: PUT
            headers:
              Authorization: "Bearer {{ consul_management_token[1:-1] }}"
            body_format: json
            body:
              Name: "{{ nomad_client_policy_name }}"
              Description: "Nomad client policy"
              Rules: "{{ nomad_client_policy_rules | to_json }}"
            return_content: true
          register: client_policy_results

        - name: Create nomad client token in consul
          uri:
            url: https://127.0.0.1:8501/v1/acl/token
            method: PUT
            headers:
              Authorization: "Bearer {{ consul_management_token[1:-1] }}"
            body_format: json
            body:
              Description: "Nomad client token"
              Policies:
                - Name: "{{ nomad_client_policy_name }}"
            return_content: true
          register: client_token_results

        - name: Debug client_token_results
          copy:
            content: "{{ client_token_results }}"
            dest: /tmp/client_token_results

        - name: Write nomad client token to Vault
          uri:
            url: "{{ vault_addr }}/v1/bootstrapper_kv/data/nomad/client"
            method: POST
            headers:
              Authorization: "Bearer {{ bootstrapper_token }}"
            body_format: json
            body:
              data:
                token: "{{ client_token_results.json.SecretID }}"
      when: >
        nomad_cluster | sort | first == ipv4_internal and
        consul_acl_enabled is true

    - name: Get nomad server token from vault
      uri:
        url: "{{ vault_addr }}/v1/bootstrapper_kv/data/nomad/server"
        headers:
          Authorization: "Bearer {{ bootstrapper_token }}"
        return_content: true
      register: nomad_server_token_result
      until: nomad_server_token_result.status == 200
      retries: 720
      delay: 5

    - name: Set nomad server token from vault
      set_fact:
        #server_token: "{{ nomad_server_token_result.json.data.data.token }}"
        server_token: "{{ consul_management_token[1:-1] }}"

    - name: Add nomad server config dir
      file:
        name: "{{ nomad_server_config_dir }}"
        state: directory
        force: yes
        owner: root
        group: nomad
        mode: "0770"

    - name: Set perms on consul client config file
      copy:
        content: This content should be replaced by consul-template
        dest: "/etc/nomad.d/nomad.hcl"
        owner: root
        group: nomad
        mode: "0660"

    - name: Copy consul-template config files to config dir
      template:
        src: templates/{{ item }}.j2
        dest: /etc/consul-template/config.d/{{ item | basename }}
        owner: root
        group: root
        mode: "0664"
      loop:
        - consul-template/nomad-server-config
      notify: Reload consul-template

    - name: Copy consul-template templates to template dir
      template:
        src: templates/{{ item }}.j2
        dest: /etc/consul-template/templates/{{ item | basename }}
        owner: root
        group: root
        mode: "0664"
      loop:
        - consul-template/nomad-server.hcl.tpl
      notify: Reload consul-template

    - name: Add nomad config file
      template:
        src: templates/nomad_server.hcl.j2
        dest: "/tmp/nomad_server.hcl"
        owner: root
        group: nomad
        mode: "0660"

    - name: Add cluster members to firewalld zone
      firewalld:
        zone: internal
        source: "{{ internal_network }}"
        permanent: true
        immediate: true
        state: enabled
      when: configure_firewalls is defined and configure_firewalls is true

    - name: Add firewalld ports to zone
      firewalld:
        zone: internal
        port: "{{ item.port }}/{{ item.proto }}"
        permanent: true
        immediate: true
        state: enabled
      loop: "{{ cluster_firewall_ports }}"
      when: configure_firewalls is defined and configure_firewalls is true

    - name: Check if nomad systemd service exists in /etc
      stat:
        path: "{{ nomad_server_service_file }}"
      register: nomad_server_service_file_check

    - name: Copy nomad systemd service to /etc
      copy:
        src: /usr/lib/systemd/system/nomad.service
        dest: "{{ nomad_server_service_file }}"
        remote_src: yes
      when: not nomad_server_service_file_check.stat.exists
      notify: Reload systemd

    - name: Change ExecStart for nomad service
      lineinfile:
        path: "{{ nomad_server_service_file }}"
        regexp: '^(ExecStart=.+-config-dir=).*'
        line: \g<1>{{ nomad_server_config_dir }}
        backrefs: yes
      notify: Reload systemd

    - name: Change ConditionFileNotEmpty to correct config file
      lineinfile:
        path: "{{ nomad_server_service_file }}"
        regexp: '^(ConditionFileNotEmpty=).*'
        line: \g<1>{{ nomad_server_config_file }}
        backrefs: yes
      notify: Reload systemd

    - name: Change EnvironmentFile to correct config dir
      lineinfile:
        path: "{{ nomad_server_service_file }}"
        regexp: '^(EnvironmentFile=-).*'
        line: \g<1>{{ config_dir }}/{{ env_file }}
        backrefs: yes
      notify: Reload systemd

    - name: Add nomad service override directory
      file:
        name: /etc/systemd/system/nomad.service.d
        state: directory

    - name: Add nomad service override file
      template:
        src: templates/nomad_systemd_override.conf.j2
        dest: /etc/systemd/system/nomad.service.d/override.conf
      notify: Reload systemd

    - name: Start and enable nomad systemd service
      systemd:
        name: nomad
        enabled: yes
      notify: Start nomad

    - name: Flush handlers
      meta: flush_handlers

    - name: Bootstrap ACLs
      block:
        - name: Wait until Nomad API returns cluster leader
          uri:
            url: https://{{ ipv4_internal }}:4646/v1/status/leader
          register: _result
          until: _result.status == 200
          retries: 720
          delay: 5

        - name: Activate ACL bootstrap through API
          uri:
            url: https://{{ ipv4_internal }}:4646/v1/acl/bootstrap
            method: POST
            return_content: true
          register: bootstrap_result

        - name: Write bootstrap token to Vault
          uri:
            url: "{{ vault_addr }}/v1/bootstrapper_kv/data/nomad/bootstrap_token"
            method: POST
            headers:
              Authorization: "Bearer {{ bootstrapper_token }}"
            body_format: json
            body:
              data:
                token: "{{ bootstrap_result.json.SecretID }}"

        - name: Check that anonymous policy exists
          assert:
            that: anonymous_policy != false
            fail_msg:
              "Anonymous policy (nomad_anonymous_policy) is required!"
            quiet: true

        - name: Create policy payload
          set_fact:
            policy_payload:
              Name: anonymous
              Description: anonymous
              Rules: '{{ anonymous_policy | to_json(indent=0) | string }}'

        - name: Debug output
          copy:
            content: "{{ policy_payload }}"
            dest: /tmp/bootstrap_output

        - name: Write anonymous policy to Nomad
          uri:
            url: "https://{{ ipv4_internal }}:4646/v1/acl/policy/anonymous"
            method: POST
            headers:
              Authorization: "Bearer {{ bootstrap_result.json.SecretID }}"
            body_format: json
            body: "{{ policy_payload }}"
            return_content: true
          ignore_errors: true
          register: policy_write

      when: >
        nomad_cluster | sort | first == ipv4_internal and
        acl_enabled is true

  handlers:
    - name: Reload systemd
      systemd:
        daemon_reload: yes

    - name: Reload consul-template
      service:
        name: consul-template
        state: reloaded

    - name: Start nomad
      service:
        name: nomad
        state: started
