---
- name: Install vault
  ansible.builtin.package:
    name: vault
    state: installed

- name: Copy vault.hcl to vault config dir
  ansible.builtin.template:
    src: templates/vault.hcl.j2
    dest: /etc/vault.d/vault.hcl
    owner: root
    group: root
    mode: "0664"
  notify: Restart vault

- name: Enable the vault service
  ansible.builtin.service:
    name: vault
    state: started
    enabled: true

- name: Include liaison tasks to get hostcert
  ansible.builtin.include_tasks:
    "tasks/liaison_hostcert.yaml"
  when: >
    initial_cert_method is defined and
    initial_cert_method == "liaison"

- name: Check status of vault
  ansible.builtin.uri:
    url: "{{ vault_api_address }}/v1/sys/health"
    return_content: true
    validate_certs: false
    status_code:
      - 200
      - 429
      - 472
      - 473
      - 501
      - 503
  register: vault_health
  retries: 5
  delay: 6

- name: Make sure vault is initialized and unsealed
  when: vault_health.status in [501, 503]
  block:
    - name: Initialize vault
      ansible.builtin.uri:
        url: "{{ vault_api_address }}/v1/sys/init"
        method: POST
        body_format: json
        body:
          secret_shares: 1
          secret_threshold: 1
        return_content: true
        validate_certs: false
      register: vault_init
      when: vault_health.status == 501

    - name: Debug vault init
      ansible.builtin.copy:
        content: "{{ vault_init }}"
        dest: /tmp/vault_init
        mode: "0644"

    - name: Include tasks to put vault keys
      ansible.builtin.include_tasks: tasks/vault_keyput.yaml
      vars:
        init_json: "{{ vault_init.json }}"
      when: vault_health.status == 501

    - name: Include tasks to get vault keys
      ansible.builtin.include_tasks: tasks/vault_keyget.yaml

    - name: Make sure vault token is set properly
      ansible.builtin.assert:
        that: "{{ (_vault_token | type_debug) == 'string' }}"
        msg: _vault_token not set to a proper value
      when: vault_token is not defined

    - name: Set vault_token to proper value
      ansible.builtin.set_fact:
        vault_token: "{{ _vault_token }}"
      when: vault_token is not defined

    - name: Make sure vault root token is set properly
      ansible.builtin.assert:
        that: "{{ (_vault_root_token | type_debug) == 'string' }}"
        msg: _vault_root_token not set to a proper value
      when: vault_root_token is not defined

    - name: Set vault_root_token to proper value
      ansible.builtin.set_fact:
        vault_root_token: "{{ _vault_token }}"
      when: vault_root_token is not defined

    - name: Make sure unseal key is set if needed
      ansible.builtin.assert:
        that: "{{ (_vault_unseal_key | type_debug) == 'AnsibleUnsafeText' }}"
        msg: >
          _vault_unseal_key is {{ _vault_unseal_key | type_debug }}
          expected AnsibleUnsafeText
      when: vault_health.status == 501

    - name: Unseal vault
      ansible.builtin.uri:
        url: "{{ vault_api_address }}/v1/sys/unseal"
        method: POST
        body_format: json
        body:
          key: "{{ _vault_unseal_key }}"
        return_content: true
        validate_certs: false
      no_log: true
      register: vault_unseal
      when: vault_health.status in [501, 503]

    - name: Debug vault_unseal
      ansible.builtin.copy:
        content: "{{ vault_unseal }}"
        dest: /tmp/vault_unseal
        mode: "0664"
      when: vault_health.status in [501, 503]

    - name: Write vault_token to environment file
      ansible.builtin.lineinfile:
        path: /var/lib/misc/vault-environmentfile
        insertafter: '^VAULT_ADDR='
        line: VAULT_TOKEN={{ vault_token | d(vault_root_token) }}
      when: >
        use_environmentfiles is true and
        vault_token == vault_root_token
